# Australian Business Lookup

## Overview

This is an application that allows you to request information and details of companies, either through ABN as well as Company Name. Company data is pulled from a 3rd party API [https://abr.business.gov.au/json/](https://abr.business.gov.au/json/).

## Requirements

This application requires the following to run:

- [Node.js][node] 12.10+
- [Yarn][yarn] 1.22+

[node]: https://nodejs.org/
[yarn]: https://yarnpkg.com/

## Running the application

Copy `.env.example` to `.env`:

```sh
cp .env.example .env
```

You will have to supply your own `ABR_SERVICE_GUID`.
You will then need to install dependencies. In the root folder run

```sh
yarn install
```

You can then launch the app by running

```sh
yarn watch
```

Runs the app in the development mode.
Open [http://localhost:1234](http://localhost:1234) to view it in the browser.

## Running unit tests

```sh
yarn test
```

This launches the test runner and runs a set of unit tests to check basic app functionality and elements.

#### Modules used

- `React` JavaScript library for building user interfaces
- `react-router-dom` for client-side routing
- `axios-jsonp-pro` for calling [ABR JSON APIs](https://abr.business.gov.au/json/)
- `enzyme` for unit testing of React components

## Assumptions

We follow the premise that the ABN number is 11 digits.
