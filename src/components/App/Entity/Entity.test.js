jest.mock("./_fetchABN");
import React from "react";
import { shallow } from "enzyme";

import Entity from "./Entity";
import fetchABN from "./_fetchABN";

describe("snapshots and behaviors", () => {
  it("should render Entity", () => {
    const component = shallow(<Entity />);
    expect(component).toMatchSnapshot();
  });

  it("should call fetchABN with current ID", () => {
    fetchABN.mockResolvedValueOnce("86097267457");
    const wrapper = shallow(
      <Entity match={{ params: { id: "86097267457" } }} />
    );
    expect(fetchABN).toHaveBeenCalledWith(1);
  });

  it.only("Company detail shows correct data", async () => {
    const company = {
      Abn: "86097267457",
      AbnStatus: "Cancelled",
      AddressDate: "2014-09-22",
      AddressPostcode: "3205",
      AddressState: "VIC",
      BusinessName: [],
      EntityName: "WINTONBAY PTY LTD",
      EntityTypeName: "Australian Private Company"
    };

    fetchABN.mockResolvedValueOnce(company);
    const wrapper = shallow(
      <Entity match={{ params: { id: "86097267457" } }} />
    );
    await wrapper.instance().busy;
    expect(wrapper.state().entity).toEqual(company);
  });
});
