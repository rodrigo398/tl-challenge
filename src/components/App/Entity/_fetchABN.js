import axios from "axios-jsonp-pro";

const fetchABN = async abn => {
  return await axios.jsonp(
    `https://abr.business.gov.au/json/AbnDetails.aspx?abn=${abn}&guid=b6242120-5bce-4b10-9839-d3045a7682da`
  );
};

export default fetchABN;
