import { Link } from "react-router-dom";
import React, { Component } from "react";

import fetchABN from "./_fetchABN";
import Spinner from "../layout/Spinner";

export class Entity extends Component {
  state = {
    entity: [],
    loading: true
  };

  componentDidMount() {
    const { match } = this.props;
    return fetchABN(match.params.id).then(res =>
      this.setState({ entity: res, loading: false })
    );
  }

  render() {
    const { entity, loading } = this.state;

    if (loading || entity.length === 0) {
      return <Spinner />;
    }

    return (
      <>
        <div className="panel-back">
          <Link to="/" className="btn btn-light">
            Back To Search
          </Link>
        </div>
        <div className="panel show-panel">
          <div className="panel-header text-center">
            <a href="/">
              <figure className="avatar" data-initial="TL"></figure>
            </a>
            <div className="panel-title h5">Company Search</div>
            <div className="panel-title h2">{entity.BusinessName[0]}</div>
            <div className="panel-subtitle">Results for ABN: {entity.Abn}</div>
          </div>
          <div className="panel-body">
            <div className="tile tile-centered">
              <div className="tile-content">
                <div className="tile-title text-bold">Entity</div>
                <div className="tile-subtitle">
                  {entity.EntityName} ({entity.EntityTypeName})
                </div>
              </div>
            </div>
            <div className="tile tile-centered">
              <div className="tile-content">
                <div className="tile-title text-bold">ABN status</div>
                <div className="tile-subtitle">
                  {entity.AbnStatus} from {entity.AddressDate}
                </div>
              </div>
            </div>

            <div className="tile tile-centered">
              <div className="tile-content">
                <div className="tile-title text-bold">
                  Main business location
                </div>
                <div className="tile-subtitle">
                  {entity.AddressPostcode}, {entity.AddressState}
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Entity;
