import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import Entity from "./Entity";
import Search from "./Search";

const App = () => {
  return (
    <BrowserRouter>
      <div className="App">
        <nav className="navbar bg-primary">
          <h1>Australian Business Lookup</h1>
        </nav>
        <Switch>
          <Route path="/entity/:id" component={Entity} />
          <Route exact path="/" component={Search} />
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export default App;
