import React from "react";
import loading from "./Loading.gif";

const Spinner = () => (
  <img
    src={loading}
    alt="Loading..."
    style={{ width: "200px", margin: "auto", display: "block" }}
  />
);

export default Spinner;
