import React from "react";
import { shallow } from "enzyme";

import Spinner from "./Spinner";
import Loading from "./Loading.gif";

test("<Spinner />", () => {
  const wrapper = shallow(<Spinner />);
  expect(wrapper.find("img").prop("src")).toEqual(Loading);
});
