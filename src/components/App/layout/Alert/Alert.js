import React from "react";

const Alert = ({ input }) => {
  return input === "" ? (
    <div className="alert alert-light">Empty Field</div>
  ) : null;
};

export default Alert;
