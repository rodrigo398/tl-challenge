import { Link } from "react-router-dom";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import React, { Component } from "react";

import { fetchByABN, fetchByEntity } from "./_fetchAPI";

const SkeletonComponent = () => {
  const skeleton = [];
  for (let index = 0; index < 4; index++) {
    skeleton.push(
      <SkeletonTheme key={index} color="#333333" highlightColor="#444">
        <section>
          <Skeleton count={3} />
          <Skeleton width={100} />
        </section>
      </SkeletonTheme>
    );
  }
  return skeleton;
};

export class Search extends Component {
  state = {
    errorMsg: "",
    input: "",
    companyList: [],
    loading: false
  };

  callAPIByABN = async () => {
    const { input } = this.state;

    try {
      this.setState({ loading: true });
      const company = await fetchByABN(input);
      this.setState({ loading: false });

      if (!company || company.Message)
        return this.setState({
          companyList: [],
          errorMsg: company.Message ? company.Message : "Entity not found!"
        });
      if (company.Abn)
        this.setState({
          companyList: [
            {
              Abn: company.Abn,
              Name: company.BusinessName[0],
              NameType: company.EntityTypeName,
              Postcode: company.AddressPostCode,
              State: company.AddressState
            }
          ],
          errorMsg: ""
        });
    } catch (err) {
      console.log(err);
    }
  };

  callAPIByName = async () => {
    const { input } = this.state;
    let errorMsg = "";
    let companyList = [];

    try {
      this.setState({ loading: true });
      const companies = await fetchByEntity(input);
      this.setState({ loading: false });
      if (!companies || companies.Names.length === 0) {
        errorMsg = "Entity not found!";
      } else {
        companyList = companies.Names;
      }

      this.setState({
        companyList: companyList,
        errorMsg: errorMsg
      });
    } catch (err) {
      console.log(err);
    }
  };

  onChange = e => {
    this.setState({
      input: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { input } = this.state;
    const abnRegx = new RegExp(/[0-9 ]{11}/);

    if (abnRegx.test(input)) {
      this.callAPIByABN();
    } else {
      this.callAPIByName();
    }
  };

  render() {
    const { errorMsg, input, companyList, loading } = this.state;

    return (
      <>
        <h1>Company Search</h1>
        <div className="subtitle">Insert ABN number or Company name:</div>
        <div className="container">
          <form className="search-panel" onSubmit={this.onSubmit}>
            <input
              type="text"
              name="text"
              placeholder="Search Company or ABN..."
              onChange={this.onChange}
            />
            <input
              className="btn btn-dark btn-block"
              type="submit"
              value="search"
            />
            <div className="panel-body">
              {errorMsg && <div className="card-body">{errorMsg}</div>}
              {!loading ? (
                !errorMsg &&
                input.length > 2 &&
                companyList.map((company, index) => (
                  <Link
                    className="card card-result"
                    key={index}
                    to={`/entity/${company.Abn}`}
                  >
                    <div className="card-body">ABN: {company.Abn}</div>
                    <div className="card-body">Name: {company.Name}</div>
                    <div className="card-body">Type: {company.NameType}</div>
                  </Link>
                ))
              ) : (
                <SkeletonComponent />
              )}
            </div>
          </form>
        </div>
      </>
    );
  }
}

export default Search;
