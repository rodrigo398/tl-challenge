import axios from "axios-jsonp-pro";

export const fetchByABN = async abn => {
  return await axios.jsonp(
    `https://abr.business.gov.au/json/AbnDetails.aspx?abn=${abn}&guid=${process.env.ABR_SERVICE_GUID}`
  );
};

export const fetchByEntity = async entity => {
  return await axios.jsonp(
    `https://abr.business.gov.au/json/MatchingNames.aspx?name=${entity}&maxResults=5&guid=${process.env.ABR_SERVICE_GUID}`
  );
};
